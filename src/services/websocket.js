import socketio from "socket.io-client";
import { uri } from "./api";

const socket = socketio(uri, { autoConnect: false });

function connect(latitude, longitude, techs) {
  socket.io.opts.query = {
    isPC: true,
    latitude,
    longitude,
    techs
  };

  socket.connect();
}

function disconnect() {
  if (socket.connected) {
    socket.disconnect();
  }
}

export { connect, disconnect };
