import axios from "axios";

// export const uri = "http://localhost:3333";
export const uri = "https://devmaps-backend.herokuapp.com";

export const api = axios.create({
  baseURL: uri + "/api"
});
