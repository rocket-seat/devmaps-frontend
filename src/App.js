import React, { useState, useEffect } from "react";

import { connect, disconnect } from "./services/websocket";
import { api } from "./services/api";
import DevItem from "./components/DevItem";
import DevForm from "./components/DevForm";

import "./global.css";
import "./App.css";
import "./Sidebar.css";
import "./Main.css";

function App() {
  const [devs, setDevs] = useState([]);
  const [palavraChave, setPalavraChave] = useState("");
  const [latitude, setLatitude] = useState(0);
  const [longitude, setLongitude] = useState(0);

  useEffect(() => {
    disconnect();

    if (latitude !== 0 && longitude !== 0) {
      connect(latitude, longitude, "");
    }
  }, [latitude, longitude]);

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude: lat, longitude: lon } = position.coords;

        setLatitude(lat);
        setLongitude(lon);
      },
      error => {},
      {
        timeout: 30000
      }
    );
  }, []);

  useEffect(() => {
    async function loadDevs() {
      const res = await api.get("/devs");
      setDevs(res.data);
    }

    loadDevs();
  }, []);

  async function handleSubmit(data) {
    const res = await api.post("/devs", data);
    setDevs([...devs, res.data]);
  }

  async function handleExcluir(id) {
    await api.delete("/devs/" + id);
    setDevs([...devs.filter(d => d._id !== id)]);
  }

  const filtrarPalavraChave = async () => {
    const res = await api.get("/search", {
      params: {
        latitude,
        longitude,
        techs: palavraChave
      }
    });

    setDevs(res.data.devs);
  };

  const limparPalavraChave = async () => {
    setPalavraChave("");

    const res = await api.get("/devs");
    setDevs(res.data);
  };

  return (
    <div id="app">
      <aside>
        <b>Cadastrar</b>
        <DevForm onSubmit={handleSubmit} />
      </aside>

      <main>
        <header>
          <input type="text" value={palavraChave} onChange={e => setPalavraChave(e.target.value)} />

          <button type="button" className="btnFiltrar" onClick={() => filtrarPalavraChave()}>
            Filtrar
          </button>

          <button type="button" className="btnLimpar" onClick={() => limparPalavraChave()}>
            Limpar
          </button>
        </header>

        <ul>
          {devs.map((dev, i) => {
            return <DevItem key={i} dev={dev} onRemove={handleExcluir} />;
          })}
        </ul>
      </main>
    </div>
  );
}

export default App;
