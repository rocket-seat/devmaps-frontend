import React from "react";

import "./styles.css";

function DevItem({ dev, onRemove }) {
  function onExcluir(id) {
    onRemove(id);
  }

  return (
    <li className="dev-item">
      <header>
        <img src={dev.avatar_url} alt={dev.name} />

        <div className="user-info">
          <b>{dev.name}</b>
          <span>{dev.techs.join(", ")}</span>
        </div>

        <button type="button" onClick={() => onExcluir(dev._id)}>
          Excluir
        </button>
      </header>

      <p>{dev.bio}</p>

      <a href={`https://github.com/${dev.github_username}`}>Acessar perfil no Github</a>
    </li>
  );
}

export default DevItem;
